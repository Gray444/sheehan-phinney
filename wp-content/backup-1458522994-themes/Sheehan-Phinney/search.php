<?php get_header(); ?>
<div class="row">
<div class="container">
    <div class="col-md-12 col-md-8">


		<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'millenniumim' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );
				?>

			<?php endwhile; ?>

			<?php millenniumim_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->


		
    </div><!-- col 12 -->

<div class="col-md-12 col-md-4">
     

<div class="sidebox-container">
<h3>Members</h3>
<ul id="members_list">
<?php
$query = new WP_Query( array( 'post_type' => array( 'member' ) ) );

while ( $query->have_posts() ) : $query->the_post();
	echo '<li><a href="';
	the_permalink();
	echo '">';
	the_title();
	echo '</a></li>';
endwhile;

?>
</ul>
</div>

   <div class="sidebox-container">
  <h3>Related News</h3>
  <?php if ( have_posts() ) : ?>
  <?php 
 

$args = array(
    	'posts_per_page' => 4,
    	'post_type' => array('related_news'),
    	'category_name' => 'employment',
    );
$loop = new WP_Query( $args );

 
while ( $loop->have_posts() ) : $loop->the_post();
?>
  <div class="sidebox-entry"><a href="<?php the_permalink(); ?>" class="sidebox-link">
  <?php the_title(); ?>
  </a></div>
  <?php endwhile;?>
  <?php endif;?>
  
</div>

  <div class="sidebox-container">
<h3>Upcoming Events</h3>

<?php
$query = new WP_Query( array( 
'posts_per_page' => 2,
'post_type' => array( 'events' ) ) );

while ( $query->have_posts() ) : $query->the_post();
	echo '<div class="sidebox-entry"><a href="';
  the_permalink();
	echo '">';
	the_title();
	echo '</a></div>';
endwhile;

?>

</div>
   
    </div> <!-- col 4 -->
    
    </div>
   
</div>

</div>


<!-- restart the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'parts/feature', 'page' ); ?>

<?php endwhile; endif; ?>


    
   
   

 

<?php get_footer(); ?>