

<?php
$query = new WP_Query( array( 'post_type' => array( 'practice' ) ) );
echo '<div class="row homepage-section col-md-12 additional-group-container">

<div class="additional_group_text_container">
<h3 style="apg_header">Additional Practice Groups</h3>';
while ( $query->have_posts() ) : $query->the_post();

	echo ' <div class="additional_group"><a href="';
	the_permalink();
	echo '">';
	the_title();
	echo '</a></div>';
endwhile;
echo '</div>
</div>
';

?>
  
  