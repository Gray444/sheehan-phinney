<div class="sidebox-container">
<h3>Education</h3>

 <?php if(get_field('education')): ?>

	
<ul class="services-list">

	<?php while(has_sub_field('education')): ?>

		<li><?php the_sub_field('degree'); ?></li>
        

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
 

</div>

