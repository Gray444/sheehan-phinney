<?php
// Get data from URL into variables
$_name = $_GET['name'] != '' ? $_GET['name'] : '';


// Start the Query
$v_args = array(
        'post_type'     =>  'member', // your CPT
        's'             =>  $_name, // looks into everything with the keyword from your 'name field'
        
    );
$vehicleSearchQuery = new WP_Query( $v_args );

// Open this line to Debug what's query WP has just run
// var_dump($vehicleSearchQuery->request);

// Show the results
if( $vehicleSearchQuery->have_posts() ) :
    while( $vehicleSearchQuery->have_posts() ) : $vehicleSearchQuery->the_post();
        the_title(); // Assumed your cars' names are stored as a CPT post title
    endwhile;
else :
    _e( 'Sorry, this', 'textdomain' );
endif;
wp_reset_postdata();
?>