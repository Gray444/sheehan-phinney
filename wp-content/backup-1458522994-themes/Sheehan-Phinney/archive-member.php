<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Millennium Base Theme
 */

get_header(); ?>
<div class="row">
<div class="col-md-12">
<div class="container">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title entry-title">Attorneys</h1>
                <?php custom_breadcrumbs(); ?>
                
           



					
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->




<?php $fields = array('name', 'location', 'alma_mater', 'position', 'bar_admission');
foreach ($fields as $field)
{
    if ($_REQUEST[$field] != '')
    {
        
        // We have something to match, otherwise ignore the field...
            $meta_query[] = array(
                'key' => $field,
                'value' => $_REQUEST[$field], // This is OK, WP_Query will sanitize input!
                'compare' => '=',
            );
    }
}
$args = array(
    'post_type' => 'members',
    'category__in' => $_REQUEST['category'],
    'posts_per_page' => -1, // -1 to display all results at once
    'order' => 'ASC',
    'meta_query' => $meta_query,
);
$query = new WP_Query($args);
?>
<div id="primary">
    <div id="content" role="main">
        <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="post">
                    <?php the_title(); ?>
                </div>
                <?php
            endwhile; //end of the loop
        endif;
        ?>
    </div><!-- #content -->
</div><!-- #primary -->
 
<div id="secondary" class="member-search" role="complementary">
    <form method="get">
 
        
        <input type="text" name="name" placeholder="First Name" value=""  id="member-search" />
        <input type="text" name="s" placeholder="Last Name" value="" />
	   <input type="text" name="position" placeholder="Position" value="" />
        <input type="text" name="alma_mater" placeholder="Alma Mater" value="" />
       <div class="selectdiv"> <select name="bar_admission">
         <option value="">BAR ADMISSION</option>
  <option value="manchester">Admission 1</option>
  <option value="boston">Admission 2</option>
  </select></div>
        <div class="selectdiv"> <select name="location">
         <option value="">Office Location</option>
  <option value="manchester">Manchester, NH</option>
  <option value="boston">Boston, MA</option>
  </select></div>
        
        <input type="submit" value="Search" />
    </form>
</div> <!--  member-search -->

<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>


				<?php if(get_field('member')): ?>
                
                
  
                
<div class="col-md-3 member-col">
	<?php while(has_sub_field('member')): ?>
    
<div class="member_list_box box" style="background-image: url(<?php the_sub_field('photo'); ?>);">
		<div class="member_overlay">
       <div class="member_box_info"><h3><?php the_title(); ?></h3>
       <div class="loc-pos"><?php the_sub_field('position'); ?> | <?php the_sub_field('member_location'); ?></div> 
       <!-- loc pos -->
       
       </div><!-- member_box_info -->
       <div class="overbox">
    <div class="title overtext">
    <a href="<?php the_permalink(); ?>">View Profile</a>
    </div><!-- overtext-->

       </div><!-- overbox -->
       
       
         </div><!--  member info -->
         </div> <!-- member box -->
	<?php endwhile; ?>

	</div><!-- col 3 -->

<?php endif; ?>



			<?php endwhile; ?>

			<?php millenniumim_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
    </div><!-- container -->
</div><!-- col 12 -->
</div> <!-- row -->

<?php get_footer(); ?>
