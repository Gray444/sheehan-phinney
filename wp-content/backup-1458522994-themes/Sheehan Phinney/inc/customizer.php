<?php
/**
 * Millennium Base Theme Theme Customizer
 *
 * @package Millennium Base Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function millenniumim_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'millenniumim_customize_register' );

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function millenniumim_customizer( $wp_customize ) {
    
	/** COMPANY INFORMATION SECTION **/
	
	$wp_customize->add_section(
        'company_info_section',
        array(
            'title' => 'Company Info',
            'description' => 'Basic information about the company',
            'priority' => 35,
        )
    );
	
	$wp_customize->add_setting(
		'phone_textbox',
		array(
			'default' => '(603) 555-1234',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'phone_textbox',
		array(
			'label' => 'Company Phone Number',
			'section' => 'company_info_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'fax_textbox',
		array(
			'default' => '(603) 555-1234',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'fax_textbox',
		array(
			'label' => 'Company Fax Number',
			'section' => 'company_info_section',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting(
		'address_textbox',
		array(
			'default' => 'Company Address',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'address_textbox',
		array(
			'label' => 'Address',
			'section' => 'company_info_section',
			'type' => 'text',
		)
	);
	
	/** SOCIAL MEDIA SECTION **/
	
	$wp_customize->add_section(
        'social_media_section',
        array(
            'title' => 'Social Media Section',
            'description' => 'Place where you can add links to various social media services',
            'priority' => 35,
        )
    );
	
	/** facebook **/
	
	$wp_customize->add_setting(
		'facebook_link',
		array(
			'default' => 'http://facebook.com',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'facebook_link',
		array(
			'label' => 'Facebook Link',
			'section' => 'social_media_section',
			'type' => 'text',
		)
	);
	
	/** twitter **/
	
	$wp_customize->add_setting(
		'twitter_link',
		array(
			'default' => 'http://twitter.com',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'twitter_link',
		array(
			'label' => 'Twitter Link',
			'section' => 'social_media_section',
			'type' => 'text',
		)
	);	
	
	/** linkedin **/
	
	$wp_customize->add_setting(
		'linkedin_link',
		array(
			'default' => 'http://linkedin.com',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'linkedin_link',
		array(
			'label' => 'linkedin Link',
			'section' => 'social_media_section',
			'type' => 'text',
		)
	);	
	
	/** youtube **/
	
	$wp_customize->add_setting(
		'youtube_link',
		array(
			'default' => 'http://youtube.com',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'youtube_link',
		array(
			'label' => 'youtube Link',
			'section' => 'social_media_section',
			'type' => 'text',
		)
	);		
	
	
	/** UPLOAD CUSTOM LOGO SECTION **/
	
	$wp_customize->add_section(
        'header_section',
        array(
            'title' => 'Logo Section',
            'description' => 'Section that defines the website\'s logo',
            'priority' => 35,
        )
    );
	
	$wp_customize->add_setting( 'logo' );

	
	$wp_customize->add_control( 
		new WP_Customize_Image_Control( 
			$wp_customize, 
			'logo', 
			array(
				'label'    => 'Upload your company\'s Logo.',
				'section'  => 'header_section',
				'settings' => 'logo',
			) 
		) 
	);	
	
	$wp_customize->add_setting( 'alt_logo' );

	
	$wp_customize->add_control( 
		new WP_Customize_Image_Control( 
			$wp_customize, 
			'alt_logo', 
			array(
				'label'    => 'If applicable, upload an alternate version of you company\'s logo.',
				'section'  => 'header_section',
				'settings' => 'alt_logo',
			) 
		) 
	);	
	
	$wp_customize->add_setting( 'accent_texture');
			
	
	$wp_customize->add_control( 
		new WP_Customize_Image_Control( 
			$wp_customize, 
			'accent_texture', 
			array(
				'label'    => 'Upload an accent texture to be used throughout the website.',
				'section'  => 'header_section',
				'settings' => 'accent_texture',
			) 
		) 
	);
    
	/** ANALYTICS SECTION **/
	
	$wp_customize->add_section(
        'analytics_section',
        array(
            'title' => 'Analytics Section',
            'description' => 'A text area to add your analytics scripts',
            'priority' => 35,
        )
    );
    
	/** analytics area **/
	
	$wp_customize->add_setting(
		'header_analytics_code',
		array(
			'default' => 'Analytics code goes here',
		)
	);

	$wp_customize->add_control(
		'header_analytics_code',
		array(
			'label' => 'Analytics Code',
			'section' => 'analytics_section',
			'type' => 'textarea',
		)
	);
    
	$wp_customize->add_setting(
		'footer_analytics_code',
		array(
			'default' => 'Analytics code goes here',
		)
	);

	$wp_customize->add_control(
		'footer_analytics_code',
		array(
			'label' => 'Analytics Code',
			'section' => 'analytics_section',
			'type' => 'textarea',
		)
	);

	/** CUSTOM FOOTER SECTION **/

    $wp_customize->add_section(
        'footer_section',
        array(
            'title' => 'Footer Section',
            'description' => 'Customize the copyright notice in the footer',
            'priority' => 35,
        )
    );
	
	$wp_customize->add_setting(
    	'hide_social-media_footer'
	);
	
	$wp_customize->add_control(
		'hide_social-media_footer',
		array(
			'type' => 'checkbox',
			'label' => 'Hide Social Media in footer',
			'section' => 'footer_section',
		)
	);	

	$wp_customize->add_setting(
		'copyright_textbox',
		array(
			'default' => 'Copyright text',
			'sanitize_callback' => 'sanitize_textbox',
		)
	);

	$wp_customize->add_control(
		'copyright_textbox',
		array(
			'label' => 'Copyright text',
			'section' => 'footer_section',
			'type' => 'text',
		)
	);
		
	
	function sanitize_textbox( $input ) {
    	return wp_kses_post( force_balance_tags( $input ) );
	}
	
}
add_action( 'customize_register', 'millenniumim_customizer' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function millenniumim_customize_preview_js() {
	wp_enqueue_script( 'millenniumim_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'millenniumim_customize_preview_js' );
