<?php
/**
 * The template for displaying all pages.
 * Template Name: Homepage
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Millennium Base Theme
 */

get_header(); ?>
<div class="row">
<?php if(get_field('homepage_banner')): ?>
<div id="carousel-example" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
      <?php $z = 0; while(the_repeater_field('homepage_banner')): ?>
      <?php $z = $z + 1; $image = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>
  <div class="<?php echo ($z==1) ? 'active ' : ''; ?>item" style="background-image: url(<?php the_sub_field('image');?>);">
 <div class="container banner-overlay">
            <div class="carousel">
              <div class="homepage-title"><?php the_sub_field('title');?></div>
              <h3><?php the_sub_field('sub_title');?></h3>
              
            </div>
          </div> </div>
      <?php endwhile; ?>

      </div>
   </div>
   <!-- /.carousel 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>-->
	<?php endif; ?>
    </div>
    


    <?php 
    if( have_rows('homepage_section') ):  
      $count = 0;
      while ( have_rows('homepage_section') ) : 
        the_row(); 
        ?>

		<div class="row homepage-section" style="background-image: url(<?php echo the_sub_field('background_image') ?>);">
    
      <div class="col-md-6 homepage-text-container <?php 
              if (!$count) {
                echo "first_homepage-section";
              }
            ?>">
      <div class="homepage-text"><?php echo the_sub_field('section_text') ?>
      
     <a href="<?php echo the_sub_field('section_button_link') ?>" target="_self"> <div class="homepage-button">
      <?php echo the_sub_field('section_button') ?>
      </div></a> <!-- button -->
      </div> <!-- text -->
      </div><!-- text container -->
       
        </div><!-- row -->
       

	 <?php 
        $count++;
      endwhile;
    endif; 
  ?>
	


<?php get_footer(); ?>