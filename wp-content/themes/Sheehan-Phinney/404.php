<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Millennium Base Theme
 */

get_header(); ?>

	<?php get_header(); ?>

<div class="row">
<div class="container-fluid">
    <div class="col-md-12">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'millenniumim' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<strong><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'millenniumim' ); ?></strong>

					<?php get_search_form(); ?>

					<br /><br />

					<div class="clearfix"></div>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</div><!-- #main -->
	</div><!-- #primary -->
    </div>
    
    

<?php get_footer(); ?>
