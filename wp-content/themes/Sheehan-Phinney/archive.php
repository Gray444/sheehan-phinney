<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Millennium Base Theme
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<header class="entry-header">
		<h1><?php post_type_archive_title(); ?></h1>

<?php custom_breadcrumbs(); ?>
		
	</header><!-- .entry-header -->
    
    <?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php millenniumim_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<?php if ( have_posts() ) : ?>
<div class="row">
<div class="container-fluid">
    <div class="col-md-12 col-md-8">
			<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'millenniumim' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'millenniumim' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'millenniumim' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'millenniumim' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'millenniumim' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'millenniumim' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'millenniumim' );

						else :
							_e( '', 'millenniumim' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php millenniumim_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
        </div>
        <div class="col-md-12 col-md-4">
     


   <div class="sidebox-container">
  <h3>Related News</h3>
  <?php if ( have_posts() ) : ?>
  <?php 
 

$args = array(
    	'posts_per_page' => 4,
    	'post_type' => array('related_news'),
    	'category_name' => 'employment',
    );
$loop = new WP_Query( $args );

 
while ( $loop->have_posts() ) : $loop->the_post();
?>
  <div class="sidebox-entry"><a href="<?php the_permalink(); ?>" class="sidebox-link">
  <?php the_title(); ?>
  </a></div>
  <?php endwhile;?>
  <?php endif;?>
  
</div>

  <div class="sidebox-container">
<h3>Upcoming Events</h3>

<?php
$query = new WP_Query( array( 
'posts_per_page' => 2,
'post_type' => array( 'events' ) ) );

while ( $query->have_posts() ) : $query->the_post();
	echo '<div class="sidebox-entry"><a href="';
  the_permalink();
	echo '">';
	the_title();
	echo '</a></div>';
endwhile;

?>

</div>
   
    </div> <!-- col 4 -->

        </div>
        </div>

		</main><!-- #main -->
	</section><!-- #primary -->


<?php get_footer(); ?>
