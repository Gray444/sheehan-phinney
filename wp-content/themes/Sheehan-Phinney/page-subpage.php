<?php
/**
 * Template Name: Sub-page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 
 ?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="row">
<div class="container-fluid">
 <div class="col-lg-8 col-md-7">


		
        

			<?php get_template_part( 'content-subpage' ); ?>

			

			
              <?php if(get_field('services')): ?>

	<h3>Our services include:</h3>
<ul class="services-list">

	<?php while(has_sub_field('services')): ?>

		<li><?php the_sub_field('service'); ?></li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
 
		
       <?php endwhile; // end of the loop. ?> 

		
    </div><!-- col 12 -->

<div class="col-lg-3 col-lg-offset-1 col-md-4 col-md-offset-1">

   
   <?php get_template_part( 'parts/subpoints', 'page' ); ?>
   
    </div> <!-- col 4 -->
    
    </div>
   
</div>

</div>


<!-- restart the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'parts/feature', 'page' ); ?>

<?php endwhile; endif; ?>


    
   
   

 

<?php get_footer(); ?>