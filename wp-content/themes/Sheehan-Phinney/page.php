<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="row">
<div class="container-fluid">
    <div class="col-md-12 col-md-8">





		
        

			<?php get_template_part( 'content' ); ?>

			

			
              <?php if(get_field('services')): ?>

	<h3>Our services include:</h3>
<ul class="services-list">

	<?php while(has_sub_field('services')): ?>

		<li><?php the_sub_field('service'); ?></li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
 
		
       <?php endwhile; // end of the loop. ?> 

		
    </div><!-- col 12 -->

<div class="col-md-12 col-md-4">
<?php if( is_page('firm' ) ){
     //either in about us, or contact, or management page is in view
get_template_part( 'parts/firm', 'page' );}
?>
     <?php
	if (is_page('industries')) {
		echo '<div class="sidebox-container">';
  echo '<h3>Industries</h3>';

   
    wp_list_pages( array(
        'title_li'    => '',
        'child_of'    => $post->ID,
        
		
    ) );
    


		echo'</div>';
	} 
?>


   <div class="sidebox-container">
  <h3>Related News</h3>
  <?php if ( have_posts() ) : ?>
  <?php 
 

$args = array(
    	'posts_per_page' => 4,
    	'post_type' => array('related_news'),
    	'category_name' => 'employment',
    );
$loop = new WP_Query( $args );

 
while ( $loop->have_posts() ) : $loop->the_post();
?>
  <div class="sidebox-entry"><a href="<?php the_permalink(); ?>" class="sidebox-link">
  <?php the_title(); ?>
  </a></div>
  <?php endwhile;?>
  <?php endif;?>
  
</div>

  <div class="sidebox-container">
<h3>Upcoming Events</h3>

<?php
$query = new WP_Query( array( 
'posts_per_page' => 2,
'post_type' => array( 'events' ) ) );

while ( $query->have_posts() ) : $query->the_post();
	echo '<div class="sidebox-entry"><a href="';
  the_permalink();
	echo '">';
	the_title();
	echo '</a></div>';
endwhile;

?>

</div>
   
    </div> <!-- col 4 -->
    
    </div>
   
</div>

</div>


<!-- restart the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'parts/feature', 'page' ); ?>

<?php endwhile; endif; ?>


    
   
   

 

<?php get_footer(); ?>