
<ul class="nav nav-tabs">
 <?php if( have_rows('key_cases') ):
     echo ' <li class="active tab-label"><a data-toggle="tab" href="#cases">Key Cases</a></li>';
endif;?>

<?php if( have_rows('admissions') ):
     echo '<li class="tab-label"><a data-toggle="tab" href="#admissions">Admissions</a></li>';
endif;?>

<?php if( have_rows('civic_involvement') ):
     echo '<li class="tab-label"><a data-toggle="tab" href="#civic">Civic Involvement</a></li>';
endif;?>

<?php if( have_rows('awards') ):
     echo '<li class="tab-label"><a data-toggle="tab" href="#awards">Awards</a></li>';
endif;?>
</ul>



<div class="tab-content">
 <?php if(get_field('key_cases')): ?>

<div id="cases" class="tab-pane fade in active">
   
    <ul>
	<?php while(has_sub_field('key_cases')): ?>

		<li><?php the_sub_field('key_case'); ?></li>
          
	<?php endwhile; ?>
 </ul>
	</div>


<?php endif; ?>
 <?php if(get_field('admissions')): ?>

  <div id="admissions" class="tab-pane fade">
    
    <ul>
	<?php while(has_sub_field('admissions')): ?>

		<li><?php the_sub_field('admission'); ?></li>
          
	<?php endwhile; ?>
 </ul>
	</div>


<?php endif; ?>
<?php if(get_field('civic_involvement')): ?>

  <div id="civic" class="tab-pane fade">
    
    <ul>
	<?php while(has_sub_field('civic_involvement')): ?>

		<li><?php the_sub_field('civic_event'); ?></li>
          
	<?php endwhile; ?>
 </ul>
	</div>


<?php endif; ?>
<?php if(get_field('awards')): ?>

  <div id="awards" class="tab-pane fade">
    
    <ul>
	<?php while(has_sub_field('awards')): ?>

		<li><?php the_sub_field('award'); ?></li>
          
	<?php endwhile; ?>
 </ul>
	</div>


<?php endif; ?>

    
</div>






