<div class="sidebox-container">
<h3>Contact Information</h3>

 <?php if(get_field('member')): ?>

	
<ul class="services-list">

	<?php while(has_sub_field('member')): ?>

		<li><span class="phone"><?php the_sub_field('phone'); ?></span></li>
        <li><span class="cell"><?php the_sub_field('cell'); ?></span></li>
        <li><span class="email"><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a> <a href="<?php the_sub_field('vcf_file'); ?>"><span class="halflings halflings-plus-sign"></span></a></span></li>
        <li><span class="location"><?php the_sub_field('member_location'); ?></span></li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
 

</div>

