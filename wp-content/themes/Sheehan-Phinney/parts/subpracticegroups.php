<div class="sidebox-container">
<h3>Sub Practice Groups</h3>






<?php 

$subareas = get_field('sub_practice_area');

if( $subareas ): ?>
	<ul class="subpractice-list">
	<?php foreach( $subareas as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
           
        </li>
    <?php endforeach; ?>
	</ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

 

</div>

