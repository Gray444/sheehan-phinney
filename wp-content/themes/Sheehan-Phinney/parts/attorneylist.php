
<?php 

$associated = get_field('associated_members');

if( $associated ): ?>
 <div class="sidebox-container">
<h3><?php the_title();?> Members</h3>
<ul id="members_list">

    <?php foreach( $associated as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <span><?php the_field('associated_members'); ?></span>
        </li>
    <?php endforeach; ?>
    </ul>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

