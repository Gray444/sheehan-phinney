
 <?php 
// check for rows (parent repeater)
				if( have_rows('tab') ): $i = 0; $count = 0;
				?>
					
                    <ul class="nav nav-tabs">
					<?php 
					
					// loop through rows (parent repeater)
					while( have_rows('tab') ): the_row(); $i++; ?>
						<li class="<?php if (!$count) {  ?>active<?php }?> tab-label"><a data-toggle="tab" href="#<?php echo $i; ?>"><?php the_sub_field('tab_title') ?></a></li>
						
					<?php $count++; endwhile; // while( has_sub_field('to-do_lists') ): ?>
					</ul>
				<?php endif; // if( get_field('to-do_lists') ): ?>


<div class="tab-content">
<?php 
// check for rows (sub repeater)
if( have_rows('tab') ): $i = 0; $count = 0;
while( have_rows('tab') ): the_row(); $i++; ?>

							<?php if( have_rows('tab_content') ):  ?>
							<div id="<?php echo $i; ?>" class="tab-pane fade  <?php if (!$count) { ?>active in<?php  } ?>">
            	<ul>
     			<?php 

								// loop through rows (sub repeater)
								while( have_rows('tab_content') ): the_row();

									// display each item as a list - with a class of completed ( if completed )
									?>
									<li><?php the_sub_field('line_item'); ?></li>
								<?php endwhile; ?>
								</ul>
                                </div>
							<?php endif; //if( get_sub_field('items') ): ?>
                          
                            <?php $count++; endwhile; endif; // while( has_sub_field('to-do_lists') ): ?>
								  </div>
						






