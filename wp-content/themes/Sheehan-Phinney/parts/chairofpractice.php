
  <div class="col-md-12">
  <div class="row chair-row">
  <div class="container">
    <?php 
global $post; // Needed for use inside a widget

$chair = get_field('chair_of_practice',$post->ID);

if( $chair): ?>
    <?php foreach( $chair as $post): // variable must be called $post (IMPORTANT) ?>
    <?php setup_postdata($post); ?>
    <div class="col-md-6">
      
      
      
      <div class="chair-photo" style="background-image: url(<?php the_field('photo'); ?>);"></div>
      <div class="chair-infobox"><h1 class="chair">Practice Area Chair</h1><br /><a href="<?php the_permalink(); ?>">
      <span class="chair-name"><?php the_title(); ?></span><br />
      </a>
     
      <?php $summary = get_field('bio');
          echo substr($summary, 0, 290); ?>
      
      <div class="chair-icons"><a href="mailto:<?php the_field('email'); ?>"><span class="halflings halflings-envelope"></span></a><a href="tel:+1-<?php the_field('phone'); ?>"><span class="halflings halflings-earphone"></span></a><a href="<?php the_permalink(); ?>"><span class="halflings halflings-user"></span></a>
      
      </div>
      </div>
      
      
    </div>
    <!-- col 6 -->
    <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endif; ?>
    </div>
  </div>
  <!-- col 12 --> 
  
</div>
<!-- row --> 

