<div class="sidebox-container">
<h3>Members</h3>
<ul id="members_list">
<?php
$query = new WP_Query( array( 'post_type' => array( 'members' ) ) );

while ( $query->have_posts() ) : $query->the_post();
	echo '<li><a href="';
	the_permalink();
	echo '">';
	the_title();
	echo '</a></li>';
endwhile;

?>
</ul>
</div>

