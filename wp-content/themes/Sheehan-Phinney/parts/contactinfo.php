<div class="sidebox-container">
<h3>Contact Information</h3>

 
	
<ul class="services-list">

	
		<?php if( get_field('phone') ): ?><li><span class="phone"><?php the_field('phone'); ?></span></li><?php endif;?>
        <?php if( get_field('cell') ): ?><li><span class="cell"><?php the_field('cell'); ?></span></li><?php endif;?>
        <li><span class="email"><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a> <a href="<?php the_field('vcf_file'); ?>"><span class="halflings halflings-plus-sign"></span></a></span></li>
        <?php if( get_field('fax') ): ?><li><span class="fax"><?php the_field('fax'); ?></span></li><?php endif;?>
        <li><span class="location"><?php
$field = get_field_object('location');
$value = get_field('location');
$label = $field['choices'][ $value ];

?>
<?php echo $label; ?></span></li>


	</ul>


 

</div>

