
<?php 
    if( have_rows('feature',$post->ID) ):  
      $count = 0;
      while ( have_rows('feature') ) : 
        the_row(); 
        ?>

		<div class="row subpage-section col-md-12" style="background-image: url(<?php echo get_sub_field('image') ?>);">
    
      <div class="col-md-6 homepage-text-container <?php 
              if (!$count) {
                echo "first_feature-section";
              }
            ?>">
      <div class="homepage-text"><?php echo get_sub_field('feature_text') ?>
      
     <a href="<?php echo get_sub_field('button_link') ?>" target="_self"> <div class="homepage-button feature-button">
      <?php echo get_sub_field('button_text') ?>
      </div></a></div><!-- button -->
      </div> <!-- text -->
      </div><!-- text container -->
      </div>
       
      
       

	 <?php 
        $count++;
      endwhile;
    endif; 
  ?>
  
  
  