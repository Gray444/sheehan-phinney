
  
  
  <?php

   $pageslug = $post->post_name; // name of the page (slug)
   $catslug = get_category_by_slug($pageslug); // get category of the same slug
   $catid = $catslug->term_id;  // get id of this category
   $query= 'cat=' . $catid. '';
   query_posts($query); // run the query
    
?>
<?php if ( have_posts() ) :?> 
<div class="sidebox-container">
  <h3>Related News</h3>
<?php while ( have_posts() ) : the_post(); ?>

    <div class="sidebox-entry"><a href="<?php the_permalink(); ?>" class="sidebox-link"><?php the_title(); ?></a></div>
<?php endwhile?>
</div>
<?php endif; ?>
<?php wp_reset_query(); ?>
