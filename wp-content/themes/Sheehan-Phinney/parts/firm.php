
  <div class="sidebox-container">
  <h3>Related Pages</h3>
  <ul>
<?php
  $page_links = get_posts(array(
    'category_name' => 'firm',
    'post_type' => 'page',
    'post_status' => 'publish',
    'orderby' => 'menu_order'
  ));

  foreach($page_links as $link){
    echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
  }
  ?>
  </ul>
  </div>
  
  

