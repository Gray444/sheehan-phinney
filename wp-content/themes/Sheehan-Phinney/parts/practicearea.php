





<?php 

$areas = get_field('practice_area');

if( $areas ): ?>
<div class="sidebox-container">
<h3>Practice Areas</h3>

	<ul>
	<?php foreach( $areas as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
           
        </li>
    <?php endforeach; ?>
    </ul>
    </div>
	
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

 



