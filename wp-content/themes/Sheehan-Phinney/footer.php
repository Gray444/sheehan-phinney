<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Millennium Base Theme
 */
?>
</div>
</div>
<!-- #content -->
<div class="row main-footer">
<div class="container">
<div class="col-sm-12 col-md-4 clearfix"> 

  
  <a  href="<?php echo home_url(); ?>" > <img src="<?php echo get_template_directory_uri(); ?>/images/logo-sheehanphinney.png" alt="Sheehan Phinney Logo"> </a>
</div>
<div class="col-sm-12 col-md-8 clearfix"> 
  <?php 
								wp_nav_menu( array( 
									'menu'=> 'footer', 
									'theme_location' => 'footer', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => 'sub_footer_menu', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?>
</div>
<!-- /.navbar-collapse -->

<div class="col-sm-12 col-sm-3 footer-location">
<span>Manchester Office</span><br />
1000 Elm Street, 17th Floor<br />
Manchester, NH 03101<br />
Phone: 603.668.0300<br />
Fax: 603.627.8121


</div> <!-- location one -->
<div class="col-sm-12 col-sm-3 footer-location">
<span>Concord Office</span><br />
Two Eagle Square, Third Floor<br /> 
Concord, NH 03301<br />
Phone: 603.223.2020<br />
Fax: 603.224.8899<br />


</div> <!-- location two -->
<div class="col-sm-12 col-sm-3 footer-location">
<span>Hanover Office</span><br />
17 1/2 Lebanon Street<br />
Hanover, NH 03755<br />
Phone: 603-643-9070<br />
Fax: 603-643-3679


</div> <!-- location three -->
<div class="col-sm-12 col-sm-3 footer-location">
<span>Boston Office</span><br /> 
255 State Street, 5th Floor<br />
Boston, MA 02109<br />
Phone: 617.897.5600<br />
Fax: 617.439.9363


</div> <!-- location four -->
</div> <!-- main footer container -->
</div> <!-- main footer row -->


<footer id="colophon" class="site-footer" role="contentinfo">
<div class="row footercurve"> </div>
<div class="row site-footer-row">
<div class="container-fluid">
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="site-info">
  <div class="col-md-12">
    <div class="col-md-12 col-md-6">
      <div class="footerinfo left-sitefooter-info"><span class="copyright">&copy; <?php echo date("Y"); ?> All Rights Reserved.</span> <?php 
								wp_nav_menu( array( 
									'menu'=> 'Sub Footer', 
									'theme_location' => 'subfooter', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => '', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?></div>
    </div>
    <div class="col-md-12 col-md-6">
      <div id="text-8" class="footerinfo right-sitefooter-info widget-container widget_text"><a href="http://www.mill-im.com" rel="designer" target="_blank">
        <div class="cred">Website powered <br />
          by the</div>
        </a> </div>
    </div>
  </div>
  <!-- .site-info --> 
</footer>
<!-- #colophon --> 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo get_template_directory_uri(); ?>/bower_components/jquery/dist/jquery.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>
<?php if (get_theme_mod('footer_analytics_code')) { echo get_theme_mod('footer_analytics_code'); } ?>

</body>
</html>
