<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Millennium Base Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="<?php echo get_template_directory_uri(); ?>/bower_components/font-awesomegit /css/font-awesome.min.css" rel="stylesheet">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />    

<?php if (get_theme_mod('header_analytics_code')) { echo get_theme_mod('header_analytics_code'); } ?>

<link href="<?php echo get_template_directory_uri(); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">   

 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>


<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom.css" media="all" type="text/css" />
<script>
jQuery(function($) {
    if($(window).width()>769){
        $('.navbar .dropdown').hover(function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function(){
            location.href = this.href;
        });

    };

	
});

</script>

<style>
@media only screen and (min-width:769px) {
.dropdown:hover .dropdown-menu {
    display: block;
}
.dropdown-submenu {
    position: relative !important;
}

.dropdown-submenu>.dropdown-menu {
    top: 0 !important;
    left: 100% !important;
    margin-top: -6px !important;
    margin-left: -1px !important;
    border-radius: 0 !important;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block !important;
}

.dropdown-submenu>a:after {
    display: block;
    content: "\f105";
    font-family: 'FontAwesome';
    margin-top: -18px;
    right: 15px;
    position: absolute;
    font-weight: 300;
}
}
</style>
</head>

<body <?php body_class(); ?>>

	
		<div class="row clearfix nav-wrapper">
			<div class="col-md-12 ">
<div class="container-fluid">
				<a class="skip-link screen-reader-text" href="#content">
					<?php _e( 'Skip to content', 'millenniumim' ); ?>
				</a>

				<header id="masthead" class="site-header" role="banner">

					<nav class="navbar navbar-default" role="navigation">
						<div class="navbar-header col-sm-12 col-md-4 clearfix">
							<!-- Brand and toggle get grouped for better mobile display -->
							
							<a class="navbar-brand col-sm-12 col-xs-10" href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo-sheehanphinney.png" alt="Sheehan Phinney Logo">  
							</a>
						</div>
                        <div class="navbar-header col-sm-12 col-md-8 clearfix">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
      <?php 
								wp_nav_menu( array( 
									'menu'=> 'primary', 
									'theme_location' => 'primary', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => '', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?>
     
    </div><!-- /.navbar-collapse -->
    </div> <!-- col 8 -->
  </div><!-- /.container-fluid -->
  
</nav>



<!-- BEGIN SEDONDARY MENU SECTION -->
<nav class="navbar secondary-nav-wrapper collapse navbar-collapse" role="navigation">
    <div class="row clearfix secondary-nav-wrapper">
			<div class="col-md-12 ">
            
	<div class="container-fluid"><div class="col-md-6 "><form method="get" id="search_form" class="search" action="<?php bloginfo('home'); ?>"/>
     <input type="submit" class="submit" value=""  />
       <input type="text" class="text" name="s" placeholder="Search..." >
      
</form>
</div>


						
						<div class="col-md-6 "><div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        
							<?php 
								wp_nav_menu( array( 
									'menu'=> 'secondary', 
									'theme_location' => 'secondary', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => '', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav secondary-menu', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?>
						</div>
                        </div>
					
    </div> <!-- container -->
    </div>
    </div>
    
    </nav>
    
   
    
    