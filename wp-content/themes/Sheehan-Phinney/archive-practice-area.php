<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Millennium Base Theme
 */

get_header(); ?>
<div class="row">
<div class="container-fluid">
    <div class="col-md-12">

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="entry-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'millenniumim' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'millenniumim' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'millenniumim' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'millenniumim' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'millenniumim' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'millenniumim' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'millenniumim' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'millenniumim' );

						else :
							_e( 'Practice Areas', 'millenniumim' );

						endif;
					?>
				</h1>
                <?php custom_breadcrumbs(); ?>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->

       
   
			<?php /* Start the Loop */ ?>
            
			

 

			<?php millenniumim_paging_nav(); ?>
<?php endif; ?>
</main><!-- #main -->
	</section><!-- #primary -->

<div class="col-md-12">

<div class="container-fluid practice-space">
 <div class="panel-group" id="accordion">


 <?php if ( have_posts(! $post->post_parent) ) : while ( have_posts() ) : the_post(); if ( ! $post->post_parent ) : // filter the pages without parent ?>
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	
<div class="col-md-3 practice-grid ">

	
   <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo get_the_ID();?>"> <div class="practice-box ">
	<div class="practice-box-title"><?php the_title() ?></div>
    
	

	</div><!--  prac box --></a>

 

    </div> <!-- col 3 -->
    

		
 </article><!-- #post-## -->

 
            <div class="panel panel-default ">
             <div id="<?php echo get_the_ID();?>" class="panel-collapse collapse sub_practice_box fade">
                
              
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> Sub Practice Groups</h1>
    <?php $sub_practice = get_field('sub_practice_area',$post->ID);

if( $sub_practice ): ?>
<ul id="sub-practice-groups_list">

	<?php foreach( $sub_practice as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
       
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
           
        
    <?php endforeach; ?>
</ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>		

</div>
</div>
<?php endif; endwhile; endif; ?>
</div> 
</div>
</div>
</div>


<?php get_footer(); ?>
