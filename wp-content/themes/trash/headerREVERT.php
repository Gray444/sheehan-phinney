<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Millennium Base Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="<?php echo get_template_directory_uri(); ?>/bower_components/font-awesomegit /css/font-awesome.min.css" rel="stylesheet">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />    

<?php if (get_theme_mod('header_analytics_code')) { echo get_theme_mod('header_analytics_code'); } ?>

<link href="<?php echo get_template_directory_uri(); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">   

 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom.css" media="all" type="text/css" />
</head>

<body <?php body_class(); ?>>

	
		<div class="row clearfix nav-wrapper">
			<div class="col-md-12 ">
	<div class="container">
				<a class="skip-link screen-reader-text" href="#content">
					<?php _e( 'Skip to content', 'millenniumim' ); ?>
				</a>

				<header id="masthead" class="site-header" role="banner">

					<nav class="navbar navbar-default" role="navigation">
						<div class="navbar-header col-sm-12 col-md-3 clearfix">
							<!-- Brand and toggle get grouped for better mobile display -->
							<button type="button" class="navbar-toggle col-xs-2" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand col-sm-12 col-xs-10" href="<?php echo home_url(); ?>" >
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo-sheehanphinney.png" alt="Sheehan Phinney Logo">  
							</a>
						</div>
						<div class="collapse navbar-collapse col-md-9 " id="bs-example-navbar-collapse-1">
							<?php 
								wp_nav_menu( array( 
									'menu'=> 'primary', 
									'theme_location' => 'primary', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => '', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?>
						</div>
					</nav>
					
				</header>
			
			</div>
		</div>
	</div>
    
    <!-- BEGIN SEDONDARY MENU SECTION -->
    <div class="row clearfix secondary-nav-wrapper">
			<div class="col-md-12 ">
	<div class="container"><form method="get" id="search_form" action="<?php bloginfo('home'); ?>"/>
     <input type="submit" class="submit" value=""  />
       <input type="text" class="text" name="s" placeholder="Search..." >
      
</form>

<nav class="navbar" role="navigation">
						<div class="navbar-header col-sm-12 col-md-3 clearfix">
							<!-- Brand and toggle get grouped for better mobile display -->
							<button type="button" class="navbar-toggle col-xs-2" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
						</div>
						<div class="collapse navbar-collapse col-md-9 " id="bs-example-navbar-collapse-2">
							<?php 
								wp_nav_menu( array( 
									'menu'=> 'secondary', 
									'theme_location' => 'secondary', 
									'depth' => 2, 
									'container' => 'div', 
									'container_class' => '', 
									'container_id' => '', 
									'menu_class' => 'nav navbar-nav secondary-menu', 
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 
									'walker' => new wp_bootstrap_navwalker()) 
								); 
							?>
						</div>
					</nav>
    </div>
    </div>
    </div>
    
    