<?php
/**
 * Template Name: Sub Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 
 ?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="row">
<div class="container">
    <div class="col-md-12 col-md-8">


		
        

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><span class="post-type-parent"><?php
echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );
?></span> <?php the_title() ?></h1>
<?php custom_breadcrumbs(); ?>
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php millenniumim_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
     <?php if(get_field('member')): ?>
	<?php while(has_sub_field('member')): ?>
<div class="member-photo" style="background-image: url(<?php the_sub_field('photo'); ?>);">
	<div class="position"><?php the_sub_field('position'); ?></div>
        </div>
        

<?php endwhile; ?>
<?php endif; ?>


 
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'millenniumim' ) ); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'millenniumim' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	
	
</article><!-- #post-## -->

			
			
              <?php if(get_field('services')): ?>

	<h3>Our services include:</h3>
<ul class="services-list">

	<?php while(has_sub_field('services')): ?>

		<li><?php the_sub_field('service'); ?></li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>

 <?php get_template_part( 'parts/achievements', 'page' ); ?>


		
       <?php endwhile; // end of the loop. ?> 

		
    </div><!-- col 12 -->

<div class="col-md-12 col-md-4">
     
  <?php get_template_part( 'parts/subpoints', 'page' ); ?>
   

    </div> <!-- col 4 -->
    
    </div>
   
   
</div>

</div>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'parts/memberlogos', 'page' ); ?>
<?php get_template_part( 'parts/video', 'page' ); ?>

<?php endwhile; endif; ?>



<!-- restart the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'parts/feature', 'page' ); ?>

<?php endwhile; endif; ?>

    
   
   

 


   

 

<?php get_footer(); ?>